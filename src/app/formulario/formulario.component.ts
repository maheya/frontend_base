import { Estudiante } from './../estudiante.model';
import { Router, ActivatedRoute } from '@angular/router';
import { EstudianteService } from './../services/estudiante-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {

  idEstudiante!: number;
  nombreInput!: string;
  ape1Input!: string;
  ape2Input!: string;
  direccionInput!: string;

  constructor(private estudianteService: EstudianteService,
              private router: Router,
              private route: ActivatedRoute) { }


  ngOnInit(): void {

    this.idEstudiante = this.route.snapshot.params['idEstudiante'];
    console.log('idEstudiante que viene del params en la ruta ---->'+ this.idEstudiante);

    if(this.idEstudiante != null){

      const estudiante = this.estudianteService.encontrarEstudiante(this.idEstudiante);
      console.log('Encontrado por id::::::::: '+estudiante)

       if(estudiante != null){

        console.log("entré a nominput para el binding")

        this.nombreInput = estudiante.nombre;
        this.ape1Input = estudiante.apellido1;
        this.ape2Input = estudiante.apellido2;
        this.direccionInput = estudiante.direccion;

       }
    }


  }


onGuardarEstudiante(){

  const estudianteAguardar = new Estudiante(this.idEstudiante, this.nombreInput, this.ape1Input,this.ape2Input,this.direccionInput);

  if(this.idEstudiante != null){

    this.estudianteService.modificarEstudiante(this.idEstudiante, estudianteAguardar);

  }else{
    this.estudianteService.agregarEstudiante(estudianteAguardar);
    console.log("Pasé el router navigate");
  }


  this.router.navigate(['estudiantes']);
}


onEliminarPersona(){
if(this.idEstudiante != null){
  console.log("Estudiante a eliminar: " + this.idEstudiante )
  this.estudianteService.eliminarEstudiante(this.idEstudiante);
}
this.router.navigate(['estudiantes']);
}


}
