import { Estudiante } from './../estudiante.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class DataService {

  constructor(private httpClient: HttpClient) {}

urlBase = 'http://localhost:8080/SiseduStudents/rest/students';



cargarEstudiantes(){
return this.httpClient.get(this.urlBase);
}


agregarEstudiante(estudiante:Estudiante){
  console.log(estudiante);
  return this.httpClient.post(this.urlBase, estudiante);
}


modificarEstudiante(idpersona : number, estudiante : Estudiante){

  let url : string;
  url = this.urlBase + '/' + idpersona;
  this.httpClient.put(url, estudiante)
  .subscribe(
    (response) => {
      console.log("Resultado de modificar estudiante: " + response);
    },
    (error) => {
      console.log("Error en la modificación de estudiante" + error);
    }
  )
}


eliminarEstudiante(idpersona : number){

  let url : string;
  url = this.urlBase + '/' + idpersona;
  this.httpClient.delete(url)
  .subscribe(
    (response) => {
      console.log("Resultado de eliminar estudiante: " + response);
    },
    (error) => {
      console.log("Error en la eliminación de estudiante" + error);
    }
  )
}





}
