import { DataService } from './data-service';
import { Estudiante } from './../estudiante.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class EstudianteService {

  estudiantes: Estudiante[] = [];

  constructor(private dataservice : DataService) { }


  // Necesitamos este método para poder inicializar el array de estudiantes y establecer su valor que vamos a recuperar posteriormente, porque la llamada a DataService es asíncrona
   setEstudiantes(estudiantes : Estudiante[]){
     this.estudiantes = estudiantes;
   }



obtenerEstudiantes(){

return this.dataservice.cargarEstudiantes();

}



   agregarEstudiante(estudiante : Estudiante): void{

    console.log("Estudiante a agregar: " + estudiante.nombre);
    this.dataservice.agregarEstudiante(estudiante)
    .subscribe(
      (response)=>{
        console.log("respuesta: " + response);

      }
    )

   }


   encontrarEstudiante(id : number){

    const estudianteBuscado : Estudiante = this.estudiantes.find( estudianteBuscado => estudianteBuscado.idStudent == id )!;   //  ! ese es el non null asertion operator, le estoy diciendo que ese valor nunca va a ser nulo o undefinido
    console.log("Estudiante encontrado: " + estudianteBuscado.idStudent + "-" + estudianteBuscado.nombre);
    return estudianteBuscado;

   }


   modificarEstudiante(id:number, estudiante : Estudiante){

    console.log("Estudiante a modificar: " + estudiante.idStudent + ".-" + estudiante.nombre);
    const estudianteModificadoLocal: any = this.estudiantes.find(estudianteArreglo => estudianteArreglo.idStudent == id);
    // Le paso a la variable local los datos que traigo del estudiante que voy a modificar si es que lo encontró en el arreglo de estudiantes general
    // estudianteModificadoLocal es el arreglo local que se está mostrando en ese momento en pantalla
    estudianteModificadoLocal.idStudent = estudiante.idStudent;
    estudianteModificadoLocal.nombre = estudiante.nombre;
    estudianteModificadoLocal.apellido1 = estudiante.apellido1;
    estudianteModificadoLocal.apellido2 = estudiante.apellido2;

    this.dataservice.modificarEstudiante(id, estudiante);
   }


   eliminarEstudiante(id:number){

    console.log("Eliminar estudiante con el ID: " + id);
    const index = this.estudiantes.findIndex(estudiante => estudiante.idStudent == id );
    this.estudiantes.splice(index,1);  //eliminado del arreglo local!!
    this.dataservice.eliminarEstudiante(id);

   }




}
