import { FormularioComponent } from './formulario/formulario.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component : EstudiantesComponent},
  {path: 'estudiantes', component : EstudiantesComponent, children:[
     {path: 'agregar', component : FormularioComponent},
     {path: ':idEstudiante', component : FormularioComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
