import { Estudiante } from './../estudiante.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { EstudianteService } from '../services/estudiante-service';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html'
})
export class EstudiantesComponent implements OnInit {


estudiantes : Estudiante[] = [];


  constructor(private estudianteService : EstudianteService,
              private router : Router,
              private route : ActivatedRoute) { }





  ngOnInit(): void {

this.estudianteService.obtenerEstudiantes()
.subscribe(
(estudiantesObtenidos : any ) => {

  this.estudiantes = estudiantesObtenidos;
  this.estudianteService.setEstudiantes(this.estudiantes);
  console.log("Estudiantes obtenidos del Subscriber: " + this.estudiantes);

})

  }




  irAgregar(){

   console.log("Nos vamos a agregar");
   this.router.navigate(['./estudiantes/agregar']);

  }




}
